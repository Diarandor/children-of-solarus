![Children of Solarus Logo](/data/logos/thumbnail.png)

# Children of Solarus

**Children of Solarus** is a free, open-source 2D game that works with
[Solarus](https://gitlab.com/solarus-games/solarus),
an open-source adventure 2D game engine.
To play this game, you need Solarus.

This project is a remake of our fangame
[Zelda Mystery of Solarus DX](https://gitlab.com/solarus-games/zsdx)
using free assets only.

See our [development blog](https://www.solarus-games.org) to get more
information and documentation about Solarus and our games.
