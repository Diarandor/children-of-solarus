# Changelog

## v1.0.0 (in progress)

* Children of Solarus edits to zsdx:
  * Edited license.txt, readme.txt
  * Edited project_db.dat and quest.dat
  * Removed languages other than English:
    * Removed data/languages/{es,zh_TW,zh_CN,de,kikoolol,fr}
    * Edited data/project_db.dat
    * Removed Chinese fonts data/text/wqy-microhei.ttc, data/text/wqy-zenhei.ttc
    * Edited data/text/fonts.dat
  * Removed non-free fonts:
    * Replaced font data/text/la.ttf by data/text/8_bit.png
    * Adjust the y coordinate of texts using it.
    * Use minecraftia.ttf font instead of 8_bit.ttf in the savegames menus.
  * Renamed Characters and Names:
    * Ganon -> Neptune:
    * Agahnim -> Lunarius:
    * Zelda -> Solaritine:
    * Rupee -> Gem:
